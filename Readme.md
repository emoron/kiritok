## Presentación
Buena tarde, a continuación se describen los requerimientos del ejercicio a partir de los cuales se realizará la evaluación técnica. 

Como mencioné en la entrevista, es un requisito que el proyecto sea desarrollado en java para el back end, particularmente utilizando Spring Boot o MVC. 
Para el front end se pueden utilizar las tecnologías de su preferencia.

## Objetivo
Evaluar los conocimientos técnicos del aspirante en el desarrollo de aplicaciones web sobre el lenguaje de programación JAVA.

## Instrucciones
El aspirante desarrollará una plataforma de reseñas de libros, que constará de lo siguiente:

- Sitio web para visualizar las reseñas de los libros, con opción a que los usuarios registrados puedan realizar comentarios en las reseñas.
- Todas y cada una de las operaciones que se realicen a la base de datos deberán de ser a través de una API, que enlazará a la aplicación web.

- Api Rest que implemente los siguientes métodos:
    * Login de usuario
    * Lista de Libros (Título, Sinopsis y Portada, mostrar de 5 en 5)
    * Detalle del Libro (Título, Sinopsis, Portada y Reseña)
    * Lista de comentarios por reseña del libro
    * Registro de comentarios
- Login de usuario, se debe ingresar el correo y la contraseña. El login solo es necesario para poder comentar la reseña de un libro
- Listado de libros, se deberán mostrar ordenadas alfabéticamente y por fecha de publicación (los más recientes primero).
- Al dar clic a un libro se deberá mostrar una vista que muestre el detalle del libro, la reseña de la misma y todos los comentarios que los usuarios han puesto en dicha reseña.
- Para realizar un comentario el usuario deberá estar previamente logueado, en caso de que no, al final del último comentario se debe mostrar un link que redirija a la pantalla de login.

## Entregable
- Instrucciones necesarias para probar el desarrollo, tomar en cuenta que la revisión puede darse en un equipo de cómputo diferente al que fue proporcionado para el desarrollo de la prueba.
- Código fuente, debidamente comentado.
- Script o Backup de la base de datos generada para el proyecto, incluir los datos correspondientes para permitir el login del usuario y el listado de libros (Título, Sinopsis, Portada, Reseña, Fecha de publicación).
- Archivos extras necesarios para poner en funcionamiento el desarrollo.
## Criterios de evaluación
- Documentación de código fuente.
- Cantidad de requerimientos completados.
- Manejo y apego al estándar http (uso de verbos).
- Limpieza en código.
- Manejo de seguridad en la aplicación.
- Estructura de base de datos.

Quedo al pendiente de cualquier duda o comentario.
Saludos cordiales.

//bookService.createBook("2020-01-01","imagen.jpg","Una buen libro para cuando estas aburrido","Cien años de soledad");