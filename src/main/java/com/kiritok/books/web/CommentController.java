package com.kiritok.books.web;

import com.kiritok.books.domain.Comments;
import com.kiritok.books.domain.Review;
import com.kiritok.books.domain.ReviewCommentPk;
import com.kiritok.books.domain.User;
import com.kiritok.books.repo.CommentRepository;
import com.kiritok.books.repo.ReviewRepository;
import com.kiritok.books.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RestController
@RequestMapping(path="/reviews/{reviewId}/comments")
public class CommentController {
    CommentRepository commentRepository;
    ReviewRepository reviewRepository;
    UserRepository userRepository;

    @Autowired

    public CommentController(CommentRepository commentRepository, ReviewRepository reviewRepository) {
        this.commentRepository = commentRepository;
        this.reviewRepository = reviewRepository;
    }

    public CommentController() {
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createCommentReview(@PathVariable(value="reviewId") int reviewId,
                                    @RequestBody @Validated CommentDto commentDto){
        User user= verifyUser(commentDto.getUserId());
        Review review = verifyReview(reviewId);
        commentRepository.save(new Comments(new ReviewCommentPk(review,commentDto.getUserId()),commentDto.getComment()));
    }

    private User verifyUser(int userId){
        return userRepository.findById(userId).orElseThrow(()->
                new NoSuchElementException("User does not exist " + userId));
    }
    private Review verifyReview(int reviewId){
        return reviewRepository.findById(reviewId).orElseThrow(()->
                new NoSuchElementException("Review does not exist " + reviewId));
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchElementException.class)
    public String return400(NoSuchElementException ex){
        return  ex.getMessage();
    }
}
