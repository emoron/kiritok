package com.kiritok.books.web;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CommentDto {
    @Size(max=255)
    private String comment;
    @NotNull
    private Integer userId;

    public CommentDto() {
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
