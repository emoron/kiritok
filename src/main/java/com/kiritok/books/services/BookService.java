package com.kiritok.books.services;

import com.kiritok.books.domain.Book;
import com.kiritok.books.domain.Review;
import com.kiritok.books.repo.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BookService {
    private BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }
    public void createBook(Date fechaPub, String portada, String sinopsis, String titulo){
        bookRepository.save(new Book(fechaPub,portada,sinopsis,titulo));

    }

    public Iterable<Book> lookup(){
        return bookRepository.findAll();
    }
}
