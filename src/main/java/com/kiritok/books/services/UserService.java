package com.kiritok.books.services;

import com.kiritok.books.domain.User;
import com.kiritok.books.repo.ReviewRepository;
import com.kiritok.books.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private UserRepository userRepository;
    //private ReviewRepository reviewRepository;
    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
       // this.reviewRepository = reviewRepository;
    }
    public User createUser(String email, String password){
        return userRepository.save(new User(email,password));
    }
    public Iterable<User> lookup(){
        return userRepository.findAll();
    }
}
