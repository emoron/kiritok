package com.kiritok.books.services;

import com.kiritok.books.domain.Book;
import com.kiritok.books.domain.Review;
import com.kiritok.books.repo.BookRepository;
import com.kiritok.books.repo.ReviewRepository;
import com.kiritok.books.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReviewService {

    private ReviewRepository reviewRepository;
    private BookRepository bookRepository;

    @Autowired


    public ReviewService(ReviewRepository reviewRepository, BookRepository bookRepository) {
        this.reviewRepository = reviewRepository;
        this.bookRepository = bookRepository;
    }


    public Review createReview(String review,Integer bookId){

       // Book book = bookRepository.findById(bookId).orElseThrow(()->new RuntimeException("Book does not exists: "+bookId));

         if(bookRepository.existsById(bookId)){
        return reviewRepository.save(new Review(review));

         }else{
            return null;
        }

    }


    public Iterable<Review> lookup(){return reviewRepository.findAll(); }

}
