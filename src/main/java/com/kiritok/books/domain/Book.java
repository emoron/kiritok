package com.kiritok.books.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Book {
    @Id
    @GeneratedValue
    @Column(name="id")
    private Integer id;
    @Column(name="publication_date")
    private Date fechaPub;
    @Column(name="book_thumb")
    private String portada;
    @Column
    private String sinopsis;
    @Column
    private String titulo;

    @OneToOne(mappedBy = "book")
    private Review review;

    public Book() {
    }

    public Book(Date fechaPub, String portada, String sinopsis, String titulo) {
        this.fechaPub = fechaPub;
        this.portada = portada;
        this.sinopsis = sinopsis;
        this.titulo = titulo;
    }

    public Integer getId() {
        return id;
    }

    public Date getFechaPub() {
        return fechaPub;
    }

    public void setFechaPub(Date fechaPub) {
        this.fechaPub = fechaPub;
    }

    public String getPortada() {
        return portada;
    }

    public void setPortada(String portada) {
        this.portada = portada;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Override
    public String toString() {
        return "{\"id\":" + id +",{\"titulo\":\"" + titulo + "\",\"fechaPub\":\"" + fechaPub  + "\",\"portada\":\"" + portada + "\",\"sinopsis\":\""+ sinopsis + "\"}";
    }
}
