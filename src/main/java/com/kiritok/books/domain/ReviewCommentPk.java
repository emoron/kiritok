package com.kiritok.books.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class ReviewCommentPk implements Serializable {
    @ManyToOne
    private Review review;

    @Column
    private Integer userId;

    public ReviewCommentPk() {
    }

    public ReviewCommentPk(Review review, Integer userId) {
        this.review = review;
        this.userId = userId;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
