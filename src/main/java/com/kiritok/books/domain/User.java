package com.kiritok.books.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String email;

    @Column
    private  String password;

//    @OneToMany(mappedBy = "USER_ID")
//    private List<Review> reviews;

    public User() {
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "id:" +id +",email:" + email + ",password:" + password;
    }
}
