package com.kiritok.books.domain;

import javax.persistence.*;

@Entity
public class Comments {

    @EmbeddedId
    private ReviewCommentPk pk;
    @Column
    private String comment;

    public Comments() {
    }

    public Comments(ReviewCommentPk pk, String comment) {
        this.pk = pk;
        this.comment = comment;
    }
}
