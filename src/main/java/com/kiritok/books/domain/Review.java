package com.kiritok.books.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Review {
    @Id
    @GeneratedValue
    @Column(name="id")
    private Integer id;

    @Column(name="review_description")
    private String review;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "book_id", referencedColumnName = "id")
    private Book book;

    public Review() {
    }

    public Review(String review) {
        this.review = review;
    }


    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

}
