package com.kiritok.books;

import com.kiritok.books.domain.Book;
import com.kiritok.books.domain.Review;
import com.kiritok.books.services.BookService;
import com.kiritok.books.services.ReviewService;
import com.kiritok.books.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootApplication
public class BooksApplication implements CommandLineRunner {
	@Autowired
	private UserService userService;
	@Autowired
	private BookService bookService;
	@Autowired
	private ReviewService reviewService;


	public static void main(String[] args) {
		SpringApplication.run(BooksApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		userService.createUser("emoron@gmail.com","12341234");
		userService.createUser("alejandraeg@gmail.com","12341234");
		userService.createUser("emoron@fismat.umich.mx","12341234");
		bookService.createBook(new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-01"),"imagen.jpg","Un buen libro para desenfadarse","Cien Años de soledad");
		bookService.createBook(new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-01"),"imagen2.jpg","Un  libro para niños","Diario de Greg");
		bookService.createBook(new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-01"),"imagen3.jpg","BestSeller Mundial","El marciano");
		reviewService.createReview("Excelente Libro",4);
		reviewService.createReview("Pesimo Libro",5);
		reviewService.createReview("No leer Libro",6);
		userService.lookup().forEach(user -> System.out.println(user));
		bookService.lookup().forEach(book -> System.out.println(book));
	}
}
