package com.kiritok.books.repo;

import com.kiritok.books.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository <User,Integer>{
}
