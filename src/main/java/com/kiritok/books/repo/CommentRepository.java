package com.kiritok.books.repo;

import com.kiritok.books.domain.Comments;
import com.kiritok.books.domain.ReviewCommentPk;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import javax.xml.stream.events.Comment;
import java.util.List;

@RepositoryRestResource(exported = false)
public interface CommentRepository extends CrudRepository<Comments, ReviewCommentPk> {
    /**
     * Localizar los comentarios de un review
     * @param reviewId is the review pk
     * @return a List of any found Comments
     */
    List<Comments> findByPk(Integer reviewId);

}
