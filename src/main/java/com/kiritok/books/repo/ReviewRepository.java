package com.kiritok.books.repo;

import com.kiritok.books.domain.Review;
import org.springframework.data.repository.CrudRepository;

public interface ReviewRepository extends CrudRepository<Review,Integer> {

}
