package com.kiritok.books.repo;

import com.kiritok.books.domain.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book,Integer> {
}
